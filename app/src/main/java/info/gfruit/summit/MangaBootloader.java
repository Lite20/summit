package info.gfruit.reader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import info.gfruit.reader.rippers.Mangakakalot;

/**
 * Created by lite20 on 9/13/2017.
 */

public class MangaBootloader {

    public static JSONObject grabIndex(String url) throws IOException {
        if(url.contains("mangakakalot.com")) {
            return new Mangakakalot().getIndex(url);
        } else {
            return null;
        }
    }

    public static JSONArray grabPages(String url) throws IOException {
        if(url.contains("mangakakalot.com")) {
            return new Mangakakalot().getPages(url);
        } else {
            return null;
        }
    }
}
