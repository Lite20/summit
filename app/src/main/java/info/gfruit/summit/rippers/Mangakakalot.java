package info.gfruit.reader.rippers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;

/**
 * Created by lite20 on 9/13/2017.
 */

public class Mangakakalot implements SiteRipper {

    @Override
    public JSONObject getIndex(String url) throws IOException, JSONException {
        JSONObject manga = new JSONObject();
        JSONArray chapters = new JSONArray();
        Document doc = Jsoup.connect(url).get();
        Elements list = doc.getElementsByClass(".chapter-list .row span a");
        for(int i = 0; i < list.size(); i++) {
            chapters.put(list.get(i).attr("href"));
        }

        manga.put("index", chapters);
        return manga;
    }

    @Override
    public JSONArray getPages(String url) throws IOException {
        JSONArray index = new JSONArray();
        Document doc = Jsoup.connect(url).get();
        Elements list = doc.getElementsByClass(".chapter-list .row span a");
        for(int i = 0; i < list.size(); i++) {
            index.put(list.get(i).attr("href"));
        }

        return index;
    }
}
