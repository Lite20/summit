package info.gfruit.reader.rippers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by lite20 on 9/13/2017.
 */

public interface SiteRipper {
    JSONObject getIndex(String url) throws IOException, JSONException;
    JSONArray getPages(String url) throws IOException;
}
